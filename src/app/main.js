import angular from 'angular';
// import classicEditor from '@ckeditor/ckeditor5-build-classic';
// import inlineEditor from '@ckeditor/ckeditor5-build-inline';
import CKEDITOR from 'dk-ckeditor5-custom-build';

CKEDITOR.InlineEditor;

import {
  sharedModule
} from './shared/shared.module';

import '../style/main.css';
import '../style/helpers.css';

angular.module('app', [
  sharedModule,
]);

angular.element(document).ready(() => {
  angular.bootstrap(document, ['app']);
});

angular.module('app').directive('ckeditor5', editorDirective);

function editorDirective () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, element, attrs, ngModel) => {
      const constructConfig = () => {
        return {};
      };
      const isTextarea = element[0].tagName.toLowerCase() === 'textarea';
      if (!isTextarea) {
        throw new Error('element is not a textarea');
      }

      // InlineEditor
      // CKEDITOR.InlineEditor
      CKEDITOR.ClassicEditor
      // classicEditor
        .create(
          element[0],
          constructConfig()
        ).then((instance) => {
          const setData = () => {
            const data = instance.getData();
            const value = ngModel.$viewValue;

            if (data !== value) {
              instance.setData(value);
            }
          };

          const setModel = () => {
            const data = instance.getData();
            ngModel.$setViewValue(data);
            ngModel.$render();
          };

          scope.$watch(() => ngModel.$viewValue, setData);
          instance.model.document.on('change', setModel);
        });
    }
  };
}
