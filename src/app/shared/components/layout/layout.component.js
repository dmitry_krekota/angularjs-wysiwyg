import template from './layout.component.html';

class LayoutComponent {
  /* @ngInject */
  constructor ($scope) {
    $scope.toolbar = [ 'blockquote' ];
    $scope.replaceToolbar = false;
    $scope.config = {
      language: 'nl'
    };
    // $scope.richModel = '<p>Text</p>';
  }
}

export default {
  template,
  controller: LayoutComponent
};
