export default class CoreService {
  /* @ngInject */
  constructor(
    $rootScope
  ) {
    this.$rootScope = $rootScope;
  }

  safeScopeApply(scope, func) {
    const digestPhase = this.$rootScope.$$phase;
    if (digestPhase !== '$apply' && digestPhase !== '$digest') {
      scope.$apply(func);
    }
  }
}
