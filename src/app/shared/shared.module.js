import angular from 'angular';
import LayoutComponent from './components/layout/layout.component';
import CoreService from './services/core/core.service';

export const sharedModule = angular
  .module('app.shared', [])
  .component('layout', LayoutComponent)
  .service('coreService', CoreService)
  .name;
